package fpat.fp.decoration.research.icons;

import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class BlockIcon extends Item implements IItemMetaSubtypes {
	
	String[] names = new String[]{"decoration_matrix"};
	
	public static final int color_matrix = 0;
	public BlockIcon()
	{
		super();
		this.setHasSubtypes(true);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack is) 
	{
		if(is.getItemDamage()<names.length)
			return super.getUnlocalizedName(is) + "_" + names[is.getItemDamage()];
		
		return super.getUnlocalizedName(is);
	}

	@Override
	public int getMaxMetas() 
	{
		return names.length;
	}

	@Override
	public String getMetaName(int meta) 
	{
		return names[meta];
	}
}
