package fpat.fp.decoration.research;

import java.io.InputStreamReader;

import futurepack.common.FPLog;
import futurepack.common.research.ResearchLoader;

public class FPATDecorationResearchLoader
{
	
	public static void init()
	{
		try
		{
			ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(FPATDecorationResearchLoader.class.getResourceAsStream("research.json")));
		}
		catch(Exception ex)
		{
			FPLog.logger.error("Failed to load research.json for FPATDecoration!");
			ex.printStackTrace();
		}
	}
}
