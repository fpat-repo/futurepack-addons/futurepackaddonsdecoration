package fpat.fp.decoration.research;

import fpat.fp.decoration.FPATDecoration;
import fpat.fp.decoration.research.icons.FPATDecorationResearchIcons;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.common.item.FPItems;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchPage;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATDecoration.modID)
public class FPATDecorationResearchPage
{

	public static ResearchPage fpatdecoration;
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	
	@SubscribeEvent
    public static void init(ResearchPageRegisterEvent event)
	{
		fpatdecoration = new ResearchPage("fpatdecoration").setIcon(new ItemStack(FPATDecorationResearchIcons.research_tab_pages, 1, 0));
	};
	
}
