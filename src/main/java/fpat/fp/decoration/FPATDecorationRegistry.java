package fpat.fp.decoration;

import fpat.fp.decoration.blocks.FPATDecorationBlocks;
import fpat.fp.decoration.blocks.FPATDecorationBlocks1;
import fpat.fp.decoration.blocks.FPATDecorationBlocks2;
import fpat.fp.decoration.blocks.FPATDecorationBlocks3;
import fpat.fp.decoration.blocks.FPATDecorationBlocks4;
import fpat.fp.decoration.blocks.FPATDecorationBlocks5;
import fpat.fp.decoration.research.icons.FPATDecorationResearchIcons;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATDecoration.modID)
public class FPATDecorationRegistry {
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		FPATDecorationBlocks.register(event);
		FPATDecorationBlocks1.register(event);
		FPATDecorationBlocks2.register(event);
		FPATDecorationBlocks3.register(event);
		FPATDecorationBlocks4.register(event);
		FPATDecorationBlocks5.register(event);
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
		event.getRegistry().registerAll(FPATDecorationBlocks.itemBlocks.toArray(new Item[FPATDecorationBlocks.itemBlocks.size()]));
		FPATDecorationBlocks.itemBlocks.clear();
		FPATDecorationBlocks.itemBlocks = null;
		event.getRegistry().registerAll(FPATDecorationBlocks1.itemBlocks.toArray(new Item[FPATDecorationBlocks1.itemBlocks.size()]));
		FPATDecorationBlocks1.itemBlocks.clear();
		FPATDecorationBlocks1.itemBlocks = null;
		event.getRegistry().registerAll(FPATDecorationBlocks2.itemBlocks.toArray(new Item[FPATDecorationBlocks2.itemBlocks.size()]));
		FPATDecorationBlocks2.itemBlocks.clear();
		FPATDecorationBlocks2.itemBlocks = null;
		event.getRegistry().registerAll(FPATDecorationBlocks3.itemBlocks.toArray(new Item[FPATDecorationBlocks3.itemBlocks.size()]));
		FPATDecorationBlocks3.itemBlocks.clear();
		FPATDecorationBlocks3.itemBlocks = null;
		event.getRegistry().registerAll(FPATDecorationBlocks4.itemBlocks.toArray(new Item[FPATDecorationBlocks4.itemBlocks.size()]));
		FPATDecorationBlocks4.itemBlocks.clear();
		FPATDecorationBlocks4.itemBlocks = null;
		event.getRegistry().registerAll(FPATDecorationBlocks5.itemBlocks.toArray(new Item[FPATDecorationBlocks5.itemBlocks.size()]));
		FPATDecorationBlocks5.itemBlocks.clear();
		FPATDecorationBlocks5.itemBlocks = null;
		
		FPATDecorationResearchIcons.register(event);
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		try
		{
			FPATDecorationBlocks.setupPreRendering();
		}
		catch(NoSuchMethodError e){}
		FPATDecorationBlocks.setupRendering();
		FPATDecorationBlocks1.setupRendering();
		FPATDecorationBlocks2.setupRendering();
		FPATDecorationBlocks3.setupRendering();
		FPATDecorationBlocks4.setupRendering();
		FPATDecorationBlocks5.setupRendering();
		FPATDecorationResearchIcons.setupRendering(); 
	}
}
