package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.block.BlockBoardComputer;

public class FPATBlockBoardComputer extends BlockBoardComputer
{

	protected FPATBlockBoardComputer() 
	{
		super();
		setCreativeTab(FPATDecoration.FPTab);
	}
}