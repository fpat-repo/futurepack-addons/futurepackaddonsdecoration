package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.FPMain;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.block.TileEntityPartPress;
import futurepack.common.gui.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IBlockMetaName;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FPATBlockPartPress extends BlockHoldingTile implements IBlockMetaName, IItemMetaSubtypes
{
	
	public FPATBlockPartPress()
	{
		super(Material.IRON,15);
		setCreativeTab(FPATDecoration.FPTab);
	}
	
	@Override
	public boolean onBlockActivated(World w, BlockPos pos, IBlockState state, EntityPlayer pl, EnumHand hand, EnumFacing face, float hitX, float hitY, float hitZ) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			pl.openGui(FPMain.instance, FPGuiHandler.PartPress, w, pos.getX(), pos.getY(), pos.getZ());
		}
		return true;
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) 
	{
		return new TileEntityPartPress();
	}
	
	
	@Override
	public void getSubBlocks(CreativeTabs t, NonNullList l) 
	{
		for(int i=0;i<16;i++)
		{
			l.add(new ItemStack(this,1,i));
		}
	}
	
	@Override
	public int getMaxMetas()
	{
		return 16;
	}

	@Override
	public String getMetaNameSub(ItemStack is) 
	{
		int i = is.getItemDamage() % EnumDyeColor.values().length;
		return EnumDyeColor.values()[i].getUnlocalizedName();

	}

	@Override
	public String getMetaName(int meta) {
		return this.getUnlocalizedName().substring(5)+"_"+meta;
	}

}
