package fpat.fp.decoration.blocks;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.FPLog;
import futurepack.common.block.BlockFlashServer;
import futurepack.common.block.BlockWandrobe;
import futurepack.common.block.FPBlocks;
import futurepack.common.block.deco.BlockDekoMeta;
import futurepack.common.block.deco.BlockDekoMetaGlass;
import futurepack.common.block.deco.BlockTreppe;
import futurepack.common.item.ItemMetaMultiTex;
import futurepack.common.item.ItemMetaSlap;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAir;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class FPATDecorationBlocks {
	
	public static List<Item> itemBlocks = new ArrayList<Item>();

	//Deco
	public static final Block colorIron = new FPATBlockhazard(Material.IRON).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("hazard_block");///*.setBlockTextureName("colorIron");

	//Part Press
	public static final Block partpress = new FPATBlockPartPress().setCreativeTab(FPATDecoration.FPTab).setHardness(3.0F).setResistance(5.0F).setUnlocalizedName("part_press");
	
	//Holo build
	public static final Block holobuild = new FPATBlockHolobuild(Material.IRON).setCreativeTab(FPATDecoration.FPTab).setHardness(3.0F).setResistance(5.0F).setUnlocalizedName("holo_build");
	

	public static void register(RegistryEvent.Register<Block> event)
	{
		
		IForgeRegistry<Block> r = event.getRegistry();

		//Hazard blocks
		registerBlockWithItem(colorIron, "hazard_block", r);
		FPBlocks.registerMetaHarvestLevel(colorIron, "axe", 1);

		//Part press
		registerBlockWithItem(partpress, "part_press", r);
		FPBlocks.registerMetaHarvestLevel(partpress, "pickaxe", 1);
		
//		//Holo build
//		registerBlockWithItem(holobuild, "holo_build", r);
//		FPBlocks.registerMetaHarvestLevel(holobuild, "pickaxe", 1);
	} 

	private static void registerBlockWithItem(Block bl,	String string, IForgeRegistry<Block> reg)
	{
		ResourceLocation res = new ResourceLocation(FPATDecoration.modID, string);
		bl.setRegistryName(res);
		reg.register(bl);
		ItemMetaMultiTex tex = new ItemMetaMultiTex(bl);
		tex.setRegistryName(res);
		itemBlocks.add(tex);
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupPreRendering()
	{
		
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{		
		try
		{			
			Field[] fields = FPATDecorationBlocks.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					Object o = f.get(null);
					if(o instanceof Block)
					{
						Item item = Item.getItemFromBlock((Block) o);
						
						if(item==null || item.getClass() == ItemAir.class)
						{
							FPLog.logger.error("Block %s has no Item!", o);						
							continue;
						}	
						
						if(o instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) o;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
			
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String s, Item i, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		ResourceLocation res = new ResourceLocation(FPATDecoration.modID, "blocks/" + toLoverCase(s));
		ModelLoader.setCustomModelResourceLocation(i, meta, new ModelResourceLocation(res, "inventory"));
		FPLog.logger.debug(String.format("Add Item %s(%s) with %s",i,meta,s));
	}

	
	private static String toLoverCase(String s)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = s.indexOf(i);
			if(index > 0)
			{
				char c = s.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				s = s.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				s = s.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return s;
	}
}