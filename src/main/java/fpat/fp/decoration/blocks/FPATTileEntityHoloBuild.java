package fpat.fp.decoration.blocks;

import futurepack.api.interfaces.ITileHologramAble;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class FPATTileEntityHoloBuild extends TileEntity implements ITileHologramAble {
	
	private IBlockState hologram = null;
	
	public IBlockState getHologram()
	{
		return hologram;
	}

	public boolean hasHologram()
	{
		return hologram!=null;
	}

	public void setHologram(IBlockState state)
	{
		hologram = state;
	}
}
