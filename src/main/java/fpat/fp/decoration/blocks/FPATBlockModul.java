package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;

public class FPATBlockModul extends futurepack.common.block.BlockModul
{
	private final int mode;

	protected FPATBlockModul(int mode)
	{	
		super(mode);
		this.mode = mode;
		setCreativeTab(FPATDecoration.FPTab);
	}
}
