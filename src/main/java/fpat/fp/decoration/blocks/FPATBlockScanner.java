package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.block.BlockScanner;

public class FPATBlockScanner extends BlockScanner
{

	protected FPATBlockScanner()
	{
		setCreativeTab(FPATDecoration.FPTab);
	}
}
