package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;
import futurepack.client.creative.TabFB_Base;
import futurepack.common.block.BlockHologram;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;


public class FPATBlockHolobuild extends BlockHologram {
	
	public FPATBlockHolobuild(Material mat)
	{
		super(mat);
		}	
	
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new FPATTileEntityHoloBuild();
	}
}
