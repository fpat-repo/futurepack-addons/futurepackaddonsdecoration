package fpat.fp.decoration.blocks;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.block.BlockAdvancedBoardcomputer;

public class FPATBlockAdvancedBoardcomputer extends BlockAdvancedBoardcomputer
{
	protected FPATBlockAdvancedBoardcomputer()
	{
		setCreativeTab(FPATDecoration.FPTab);
	}
}