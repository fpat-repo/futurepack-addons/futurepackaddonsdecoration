package fpat.fp.decoration.blocks;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fpat.fp.decoration.FPATDecoration;
import futurepack.common.FPLog;
import futurepack.common.block.BlockFlashServer;
import futurepack.common.block.BlockWandrobe;
import futurepack.common.block.FPBlocks;
import futurepack.common.block.deco.BlockDekoMeta;
import futurepack.common.block.deco.BlockDekoMetaGlass;
import futurepack.common.block.deco.BlockTreppe;
import futurepack.common.item.ItemMetaMultiTex;
import futurepack.common.item.ItemMetaSlap;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAir;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class FPATDecorationBlocks2 {
	
	public static List<Item> itemBlocks = new ArrayList<Item>();

	//Wardrobe
	public static final Block wardrobe2 = new BlockWandrobe(Material.IRON).setCreativeTab(FPATDecoration.FPTab).setHardness(3.0F).setResistance(5.0F).setUnlocalizedName("wardrobe");
	//Machines
	//Flash Servers
	public static final Block flashserver2 = new BlockFlashServer().setCreativeTab(FPATDecoration.FPTab).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("flashserver");

	//Board Computer
	public static final Block boardcomputer2 = new FPATBlockBoardComputer().setCreativeTab(FPATDecoration.FPTab).setHardness(3.0F).setResistance(5.0F).setUnlocalizedName("board_computer");

//	//Advanced Board Computer red
//	public static final Block advancedboardcomputerred2 = new FPATBlockAdvancedBoardcomputer().setUnlocalizedName("advanced_board_computer").setHardness(3.0F).setResistance(5.0F);
//	
//	//Advanced Board Computer blue
//	public static final Block advancedboardcomputerblue2 = new FPATBlockAdvancedBoardcomputer().setUnlocalizedName("advanced_board_computer").setHardness(3.0F).setResistance(5.0F);
//	
//	//Advanced Board Computer green
//	public static final Block advancedboardcomputergreen2 = new FPATBlockAdvancedBoardcomputer().setUnlocalizedName("advanced_board_computer").setHardness(3.0F).setResistance(5.0F);
	
	//Modules
	public static final Block modul1_2 = new FPATBlockModul(1).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("modul.1");
	
	public static final Block modul2_2 = new FPATBlockModul(2).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("modul.2");
	
	public static final Block modul3_2 = new FPATBlockModul(3).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("modul.3");
	
	public static final Block modul4_2 = new FPATBlockModul(4).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("modul.4");

	//Scanner
	public static final Block scanner = new FPATBlockScanner().setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("scanner");

	public static void register(RegistryEvent.Register<Block> event)
	{
		
		IForgeRegistry<Block> r = event.getRegistry();

		//Wardrobes
		registerBlockWithItem(wardrobe2, "wardrobe_2", r);
		FPBlocks.registerMetaHarvestLevel(wardrobe2, "pickaxe", 1);
		
		//Flash servers
		registerBlockWithItem(flashserver2, "flash_server_2", r);
		FPBlocks.registerMetaHarvestLevel(flashserver2, "pickaxe", 1);
		
		//Board computers
		registerBlockWithItem(boardcomputer2, "board_computer_2", r);
		FPBlocks.registerMetaHarvestLevel(boardcomputer2, "pickaxe", 1);
		
		//Modules
		registerBlockWithItem(modul1_2, "modul1_2", r);
		FPBlocks.registerMetaHarvestLevel(modul1_2, "pickaxe", 1);
		
		
		
		registerBlockWithItem(modul2_2, "modul2_2", r);
		FPBlocks.registerMetaHarvestLevel(modul2_2, "pickaxe", 1);
		
		
		
		registerBlockWithItem(modul3_2, "modul3_2", r);
		FPBlocks.registerMetaHarvestLevel(modul3_2, "pickaxe", 1);
		
		

		registerBlockWithItem(modul4_2, "modul4_2", r);
		FPBlocks.registerMetaHarvestLevel(modul4_2, "pickaxe", 1);
		
//		//Advanced Computers
//		registerBlockWithItem(advancedboardcomputerred2, "advancedboardcomputerred_2", r);
//		FPBlocks.registerMetaHarvestLevel(advancedboardcomputerred2, "pickaxe", 1);
//		
//		
//
//		registerBlockWithItem(advancedboardcomputerblue2, "advancedboardcomputerblue_2", r);
//		FPBlocks.registerMetaHarvestLevel(advancedboardcomputerblue2, "pickaxe", 1);
//		
//		
//
//		registerBlockWithItem(advancedboardcomputergreen2, "advancedboardcomputergreen_2", r);
//		FPBlocks.registerMetaHarvestLevel(advancedboardcomputergreen2, "pickaxe", 1);

		//Scanner
		registerBlockWithItem(scanner, "scanner_2", r);
		FPBlocks.registerMetaHarvestLevel(scanner, "pickaxe", 1);
	} 

	private static void registerBlockWithItem(Block bl,	String string, IForgeRegistry<Block> reg)
	{
		ResourceLocation res = new ResourceLocation(FPATDecoration.modID, string);
		bl.setRegistryName(res);
		reg.register(bl);
		ItemMetaMultiTex tex = new ItemMetaMultiTex(bl);
		tex.setRegistryName(res);
		itemBlocks.add(tex);
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupPreRendering()
	{
		
	}
	
	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{		
		try
		{			
			Field[] fields = FPATDecorationBlocks2.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					Object o = f.get(null);
					if(o instanceof Block)
					{
						Item item = Item.getItemFromBlock((Block) o);
						
						if(item==null || item.getClass() == ItemAir.class)
						{
							FPLog.logger.error("Block %s has no Item!", o);						
							continue;
						}	
						
						if(o instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) o;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i) + "_2", item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
			
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String s, Item i, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		ResourceLocation res = new ResourceLocation(FPATDecoration.modID, "blocks/" + toLoverCase(s));
		ModelLoader.setCustomModelResourceLocation(i, meta, new ModelResourceLocation(res, "inventory"));
		FPLog.logger.debug(String.format("Add Item %s(%s) with %s",i,meta,s));
	}

	
	private static String toLoverCase(String s)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = s.indexOf(i);
			if(index > 0)
			{
				char c = s.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				s = s.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				s = s.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return s;
	}
}