package fpat.fp.decoration;

import java.io.File;

import fpat.fp.decoration.creativetab.FPATDecorationTab;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;


@Mod(modid = FPATDecoration.modID, name = FPATDecoration.modName, version = FPATDecoration.modVersion, dependencies = "required-after:fp;" + "required-after:fpatcore")
public class FPATDecoration
{
	public static final String modID = "fpatdecoration";
	public static final String modName = "Futurepack Addon - Decoration";
	public static final String modVersion = "Version.version";
	
	@Instance(FPATDecoration.modID)
	public static FPATDecoration instance;

//	public static ConfigHandler configHandler;
	
	public FPATDecoration()
	{

	}
	

	public static TabFB_Base FPTab = new FPATDecorationTab(CreativeTabs.getNextID(), "FPATDecorationTab");


	@SidedProxy(modId=FPATDecoration.modID, clientSide="fpat.fp.decoration.FPATDecorationProxyClient", serverSide="fpat.fp.decoration.FPATDecorationProxyServer")
	public static FPATDecorationProxyBase proxy;

	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {	
//		File configFile = new File(event.getModConfigurationDirectory(), "fpat/decoration/main.conf");
//		configHandler = new ConfigHandler(configFile);
//		configHandler.addConfigurable(new ConfigurationMain());
    	proxy.preInit(event);
    }
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
//		configHandler.reload(true);
		proxy.load(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}