package fpat.fp.decoration;

import java.util.ArrayList;
import java.util.List;

import fpat.fp.decoration.research.FPATDecorationResearchLoader;
import futurepack.common.FPLog;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class FPATDecorationProxyBase {

	
	public void preInit(FMLPreInitializationEvent event) 
	{
		FPLog.initLog();
	
	}

	public void load(FMLInitializationEvent event)
	{

//	    GameRegistry.registerTileEntity(FPATileEntityWardrobeSmall.class, "fpa_wandrobe_small_");
//	    GameRegistry.registerTileEntity(FPATileEntityWardrobeLarge.class, "fpa_wandrobe_large_");
	    
	    FPATDecorationResearchLoader.init();
	}
	
	public void postInit(FMLPostInitializationEvent event)
	{
		
	}
	
}
