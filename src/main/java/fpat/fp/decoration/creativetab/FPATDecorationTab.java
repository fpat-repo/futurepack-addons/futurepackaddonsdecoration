package fpat.fp.decoration.creativetab;

import fpat.fp.decoration.research.icons.FPATDecorationResearchIcons;
import futurepack.client.creative.TabFB_Base;
import futurepack.common.block.FPBlocks;

import net.minecraft.item.ItemStack;

public class FPATDecorationTab extends TabFB_Base {

	public FPATDecorationTab(int id, String label) 
	{
		super(id, label);
	}

	
	@Override
	public ItemStack getTabIconItem()
	{
		return new ItemStack(FPATDecorationResearchIcons.research_tab_pages,1,0);
	}
}
